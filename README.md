# webmagicx

### webmagicx一款基于webmagic的可配置化的爬虫框架


webmagicx是一款可配置爬虫框架，webmagicx中的webmagic表示该框架扩展于webmagic，x表示该框架是一个基于xml的配置型爬虫框架。
得益于webmagic强大的可扩展能力，本框架实现了以下特性：


- 无需写任何代码，只需你熟悉正则表达式和xpath，通过简单的配置便可实现一个爬虫。
- 实现了基于corn的定时调度功能。
- 实现的简单通用的管道，能够将抓取的数据轻松存入数据库和文件。


### 快速开始

 **使用二进制包开始第一个爬虫** 

webmagicx提供了可执行的二进制包，免去了编译痛苦，直接下载就可以使用。你只需要以下步骤，就可以启动你的第一个爬虫:

- 首先你需要安装jdk1.8以上哦
- [点击此处下载webmagicx二进制包](https://gitee.com/luosl/webmagicx/attach_files/download?i=110431&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F02%2F95%2FPaAvDFpA8ciAW7L0A3TF98WaQnM196.zip%3Ftoken%3D9593933320d105f6dd4043db8a485fc9%26ts%3D1514252817%26attname%3Dwebmagicx.zip)
- 解压zip包，运行start.bat，或者在当前目录执行命令 java -jar webmagicx-1.0-SNAPSHOT.jar
- 待爬虫运行一段时间后会在当前目录生成 test.csv 文件，文件内便是爬虫抓取的信息。
- 打开浏览器，输入网址[http://localhost:9000/spider/osc.spider](http://localhost:9000/spider/osc.spider),即可看到爬虫的运行状态，如下图所示:
- ![爬虫运行信息](https://gitee.com/uploads/images/2017/1225/155703_51b9189f_142580.png "QQ截图20171225155648.png")

 **webmagicx的结构** 

webmagicx的二进制包解压后会得到如下的一些文件:

```
webmagicx
└ spiderConf                      → 爬虫配置文件目录，用于存放爬虫配置文件
  └ osc.spider.xml                → 爬虫配置文件，请放到格式为xml，请放到spiderConf目录下，请务必以.spider.xml结尾
└ start.bat                       → windos环境下，快捷启动命令,可用 java -jar webmagicx-1.0-SNAPSHOT.jar 命令代替
└ webmagicx-1.0-SNAPSHOT.jar      → webmagix二进制jar包
```

 **webmagicx的配置文件** 

爬虫配置是webmagicx的核心，至于配置文件选用xml格式的原因有以下几点:


- xml非常的大众化，特别是对于广大的javaer而言。
- xml虽然有些臃肿，但它的表达能力是毋庸置疑的，同时他也非常易于阅读，同时层次也非常的清晰。
- 最后一点，我觉得scala解析xml真的很容易 :sunglasses: 

说了这么多，其实配置也就这几个部分：


- startUrls：爬虫url入口。
- targetUrlRegexs：爬虫到底抓那些网页？
- attribute：爬虫的属性，比如开几个线程，抓多深的深度等。
- components：爬虫的组件爬虫，一般情况下，我们只需要定义pipeline，用来告诉爬虫怎么保存抓取的结果。
- site：站点配置，主要是用来伪装爬虫。
- fields：抓取的字段配置。

列举了这么，其实下面的配置文件就说的很清楚了。。。。

```
<?xml version="1.0" encoding="UTF-8" ?>
<spider enable="true">
    <!-- 任务描述 -->
    <desc>抓取开源中国博客的标题和作者</desc>
    <!-- 定时任务配置,当startNow为true，任务会马上启动  -->
    <task startNow="true" corn="0 0 */1 * * ?" />
    <!-- 起始url列表，可以配置多个 -->
    <startUrls>
        <url>https://www.oschina.net/blog</url>
    </startUrls>
    <!-- 目标url正则，可以配置多个 -->
    <targetUrlRegexs>
        <regex xpath="//*section[@class='box-aw']">https://my\.oschina\.net/[a-z0-9A-Z]+/blog.*</regex>
    </targetUrlRegexs>
    <!-- 爬虫属性 -->
    <attribute>
        <!-- 最大抓取深度 -->
        <maxDeep>0</maxDeep>
        <!-- 字符集 -->
        <charset>UTF-8</charset>
        <!-- 超时(毫秒) -->
        <timeout>20000</timeout>
        <!-- 线程数 -->
        <threadNum>15</threadNum>
        <!-- 抓取失败后的重试次数 -->
        <retryTimes>3</retryTimes>
    </attribute>
    <!-- 组件配置 -->
    <components>
        <!-- 管道配置 -->
        <!-- jdbc管道 -->
        <!--<pipeline>-->
            <!--<class>org.luosl.webmagicx.pipeline.SimpleJdbcPipeline</class>-->
            <!--<props>-->
                <!--&lt;!&ndash; 支持override和append，分别代表覆盖和追加 &ndash;&gt;-->
                <!--<model value="append"/>-->
                <!--&lt;!&ndash; 用来进行去重的字段，在 程序中内置[_url,_page]两个字段，分别表示当前url，和当前页面 &ndash;&gt;-->
                <!--<distinctField value="url"/>-->
                <!--&lt;!&ndash; 需要保存的字段,*表示所有字段 &ndash;&gt;-->
                <!--<needSaveFields value="url,content,abstruct,startTime,tag,pollutionLevel"/>-->
                <!--&lt;!&ndash; 数据库驱动 &ndash;&gt;-->
                <!--<driver value="com.mysql.jdbc.Driver"/>-->
                <!--&lt;!&ndash; 数据库url &ndash;&gt;-->
                <!--<url>jdbc:mysql://localhost/cetc_spider?useUnicode=true&amp;characterEncoding=UTF-8</url>-->
                <!--&lt;!&ndash; 数据库用户名 &ndash;&gt;-->
                <!--<user value="root"/>-->
                <!--&lt;!&ndash; 数据库密码 &ndash;&gt;-->
                <!--<password value="123456"/>-->
                <!--&lt;!&ndash; 数据库表名称 &ndash;&gt;-->
                <!--<tableName  value="tb_alert"/>-->
            <!--</props>-->
        <!--</pipeline>-->
        <!-- csv管道 -->
        <pipeline>
            <class>org.luosl.webmagicx.pipeline.CSVPipeline</class>
            <props>
                <!-- 支持override和append，分别代表覆盖和追加 -->
                <model value="override"/>
                <!-- 用来进行去重的字段，在 程序中内置[_url,_page]两个字段，分别表示当前url，和当前页面 -->
                <distinctField value="_url"/>
                <!-- 需要保存的字段,*表示所有字段 -->
                <needSaveFields value="_url,title,author"/>
                <!-- 在爬虫任务终止时是否需要关闭csv文件，当任务为一次性时请设置为true，为调度任务时，设置为false -->
                <closeAtTheEnd value="false" />
                <!-- 保存的csv路径 -->
                <path value="test.csv"/>
            </props>
        </pipeline>
    </components>
    <!-- 站点配置 -->
    <site>
        <!-- 浏览器标识 -->
        <userAgent>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36</userAgent>
        <!-- 请求头 -->
        <headers>
            <header key="authority" value="www.oschina.net" />
        </headers>
        <!-- cookie -->
        <cookies>
            <cookie key="" value=""/>
        </cookies>
    </site>
    <!-- 抓取字段配置 -->
    <fields>
        <field>
            <!-- 字段名称 -->
            <name>author</name>
            <!-- 抽取html的xpath表达式 -->
            <xpath>//*div[@class='user-info']/div[@class='name']/a/text()</xpath>
            <!-- 是否格式化为纯文本 -->
            <textFormat>true</textFormat>
            <!-- 是否为必须字段，当为true时，若该字段未抓取到值，不会进行保存 -->
            <must>true</must>
        </field>
        <field>
            <name>title</name>
            <!--
            一个字段有多个xpath表达式的写法，scope取值为head，last，all；
            分别表示：取第一个有效值；取最后一个有效值，取所有有效值
            -->
            <xpaths scope="head">
                <xpath>//*div[@class='title']/text()</xpath>
                <!--<xpath>//*div[@class='title']/text()</xpath>-->
            </xpaths>
            <textFormat>true</textFormat>
            <must>true</must>
        </field>
    </fields>
</spider>

```

### 说明

项目才刚刚开始，大家有什么建议和想法欢迎一起交流。同时也希望有兴趣和精力的盆友一起来完善这个项目 :smile: 

### QQ群

468248192
