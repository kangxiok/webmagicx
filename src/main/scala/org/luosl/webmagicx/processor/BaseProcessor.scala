package org.luosl.webmagicx.processor

import java.io.Closeable

import org.luosl.webmagicx.Utils.Logging
import org.luosl.webmagicx.conf.{Props, SpiderConf}
import org.luosl.webmagicx.listeners.ProcessorListener
import us.codecraft.webmagic.{Page, Site}
import us.codecraft.webmagic.processor.PageProcessor

import scala.collection.mutable.ArrayBuffer

/**
  * Created by luosl on 2017/11/7.
  */
abstract class BaseProcessor(sc:SpiderConf,props:Props) extends PageProcessor with Logging with Closeable{

  /**
    * 监听器
    */
  protected var listeners:ArrayBuffer[ProcessorListener] = ArrayBuffer.empty[ProcessorListener]

  private val site:Site = {
    val sites:Site = Site.me()
    sites.setCharset(sc.attribute.charset) // 设置字符集
    sites.setTimeOut(sc.attribute.timeout) // 设置超时
    sites.setCycleRetryTimes(sc.attribute.retryTimes)
    sites.setUserAgent(sc.site.userAgent)
    sc.site.headers.foreach(kv=>sites.addHeader(kv._1,kv._2))
    sc.site.cookies.foreach(kv=>sites.addCookie(kv._1,kv._2))
    sites
  }

  /**
    * 添加一个监听器
    * @param listener listener
    * @return
    */
  def addListener(listener:ProcessorListener):BaseProcessor = {
    listeners += listener
    this
  }

  def onSkip(page:Page): Unit ={
    listeners.foreach(ls=>ls.onSkip(page))
  }

  def onError(page:Page): Unit ={
    listeners.foreach(ls=>ls.onError(page))
  }

  def onSuccess(page:Page): Unit ={
    listeners.foreach(ls=>ls.onSuccess(page))
  }

  override def getSite: Site = site

  override def close(): Unit ={
  }
}
