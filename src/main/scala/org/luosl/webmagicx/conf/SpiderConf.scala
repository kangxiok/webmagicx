package org.luosl.webmagicx.conf

import org.apache.commons.io.FilenameUtils
import us.codecraft.webmagic.scheduler.Scheduler

/**
  * 爬虫配置信息
  *
  * @param startUrls 起始url
  * @param targetUrlRegexs 目标url正则
  * @param components 组件
  * @param fields 字段
  * @param site 站点信息
  * @param attribute 爬虫属性
  */
case class SpiderConf(
                       desc:String,
                       startUrls:Seq[String],
                       targetUrlRegexs: Seq[TargetUrlRegex],
                       task:Option[Task],
                       components: Components,
                       fields:Seq[Field],
                       site: Site,
                       attribute: Attr,
                       path:String,
                       enabled: Boolean
                      ){
  val id:String = FilenameUtils.getBaseName(path)
  val taskType:String = task.map(t=>s"corn;${t.corn}").getOrElse("once")
}

/**
  * 目标正则描述
  * @param xpath xpath
  * @param regex 正则
  */
case class TargetUrlRegex(xpath:Option[String],regex:String)

/**
  * 爬虫字段配置
  * @param name 字段名称
  * @param xpaths xpath
  * @param must 是否必须
  */
case class Field(name:String, xpaths:Seq[String], scope:String, textFormat:Boolean, must:Boolean)

/**
  * 站点信息配置
  * @param userAgent 浏览器标识
  * @param headers 请求头
  * @param cookies cookie
  */
case class Site(userAgent:String,headers:Map[String,String],cookies:Map[String,String])

/**
  * 组件
  * @param clazz clazz
  * @param  prop prop
  */
case class Component(clazz:String,prop:Map[String,String])
/**
  * 组件集合
  * @param processor 处理器
  * @param pipelines 持久化管道
  */
case class Components(processor:Option[Component], handlers:Seq[Component],pipelines:Seq[Component], scheduler: Option[Component])

/**
  *
  * @param maxDeep 最大深度
  * @param charset 字符集
  * @param timeout 超时
  * @param threadNum 线程数
  * @param retryTimes 重试次数
  */
case class Attr(maxDeep:Int, charset:String, timeout:Int, threadNum:Int, retryTimes:Int)

/**
  * 任务
  * @param corn corn
  */
case class Task(startNow:Boolean, corn:String)
