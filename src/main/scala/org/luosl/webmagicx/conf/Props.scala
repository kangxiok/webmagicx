package org.luosl.webmagicx.conf

/**
  *
  * @param props 属性
  * @param confPath 配置路径
  * @param compName 组件名称
  */
class Props(props: Map[String,String], confPath:String, compName:String) {
  /**
    * 获取值或抛出异常
    * @param key key
    * @return
    */
  def prop(key:String):String = {
    props.getOrElse(key,
      throw new RuntimeException(s"组件:$compName，" +
        s"需要必要的属性[name=$key]，请检查配置文件:$confPath"))
  }

  def propOption(key:String):Option[String] = props.get(key)

  def prop(key:String, defVal:String): String = props.getOrElse(key,defVal)

  def propOrNull(key:String): String = prop(key, null)
}

object Props{

  def apply(props: Map[String, String], confPath: String, compName: String): Props =
    new Props(props, confPath, compName)

  def apply(confPath: String, compName: String): Props = Props(Map.empty[String, String], confPath, compName)
}
