package org.luosl.webmagicx.Utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

/**
  * Created by luosl on 2017/12/11.
  */
object DateUtils {

  def dateFormat(date:Date, format:String): String = {
    val sdf:SimpleDateFormat = new SimpleDateFormat(format)
    sdf.format(date)
  }

  def str2Date(str:String, format:String): Date ={
    val sdf:SimpleDateFormat = new SimpleDateFormat(format)
    sdf.parse(str)
  }

  def getYear(date: Date): Int ={
    val c:Calendar = Calendar.getInstance()
    c.setTime(date)
    c.get(Calendar.YEAR)
  }

  def getNowYear():Int = getYear(new Date())

  def dateFormat(date:Date): String = dateFormat(date, "yyyy-MM-dd HH:mm:ss")

}
