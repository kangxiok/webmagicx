package org.luosl.webmagicx.Utils

import java.util.regex.Pattern

import scala.collection.mutable.ArrayBuffer

/**
  * Created by luosl on 2017/5/3.
  */
object RegexUtils {
  def find(str:String,regex:String):ArrayBuffer[String] = {
    val buffer = ArrayBuffer.empty[String]
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(str)
    while (matcher.find) {
      buffer += matcher.group
    }
    buffer
  }
}
