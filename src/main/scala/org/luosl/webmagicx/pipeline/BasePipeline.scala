package org.luosl.webmagicx.pipeline

import java.io.Closeable

import org.luosl.webmagicx.Utils.Logging
import org.luosl.webmagicx.conf.{Props, SpiderConf}
import org.luosl.webmagicx.listeners.PipelineListener
import us.codecraft.webmagic.{ResultItems, Task}
import us.codecraft.webmagic.pipeline.Pipeline

import scala.collection.mutable.ArrayBuffer

/**
  * Created by luosl on 2017/11/7.
  */
abstract class BasePipeline(sc:SpiderConf, props:Props) extends Pipeline with Logging with Closeable{

  /**
    * 监听器
    */
  protected var listeners:ArrayBuffer[PipelineListener] = ArrayBuffer.empty[PipelineListener]

  /**
    * 添加一个监听器
    * @param listener listener
    * @return
    */
  def addListener(listener:PipelineListener):BasePipeline = {
    listeners += listener
    this
  }

  def onSkip(resultItems: ResultItems, task: Task): Unit ={
    listeners.foreach(ls=>ls.onSkip(resultItems, task))
  }

  def onError(resultItems: ResultItems, task: Task): Unit ={
    listeners.foreach(ls=>ls.onError(resultItems, task))
  }

  def onSuccess(resultItems: ResultItems, task: Task): Unit ={
    listeners.foreach(ls=>ls.onSuccess(resultItems, task))
  }

  override def close(): Unit ={
  }

}
