package org.luosl.webmagicx.pipeline.component

import org.luosl.webmagicx.Utils.Logging
import us.codecraft.webmagic.ResultItems

/**
  * Created by luosl on 2017/12/14.
  */
abstract class Distinct(distinctValue:ResultItems => Any) extends Logging{
  /**
    * 添加一个 item 到去重器
    * @param item  item
    */
  def addItem(item: Any):Unit

  /**
    * 判断一个 item 是否为唯一
    * @param item item
    * @return
    */
  def isUnique(item:Any):Boolean

  /**
    * 判断一个 item 是否为唯一，当isUnique为true时 将调用addItem
    * @param items items
    * @return
    */
  def isUniqueAndAdd(items:ResultItems):Boolean = {
    val value:Any = distinctValue(items)
    val unique:Boolean = isUnique(value)
    if(unique) {
      addItem(value)
    }
    unique
  }
}
