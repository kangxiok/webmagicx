package org.luosl.webmagicx.scheduler

import java.io.Closeable

import org.luosl.webmagicx.conf.{Props, SpiderConf}
import us.codecraft.webmagic.scheduler.component.HashSetDuplicateRemover

/**
  * Created by luosl on 2017/12/6.
  */
class QueueScheduler(sc:SpiderConf, props: Props) extends us.codecraft.webmagic.scheduler.QueueScheduler with Closeable{
  override def close(): Unit = {
    this.setDuplicateRemover(new HashSetDuplicateRemover)
  }
}
