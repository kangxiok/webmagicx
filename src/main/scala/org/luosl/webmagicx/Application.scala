package org.luosl.webmagicx

import java.util.concurrent.TimeUnit

import org.luosl.webmagicx.Utils.Logging
import org.luosl.webmagicx.cli.{Cli, CliProp}
import org.luosl.webmagicx.conf._
import us.codecraft.webmagic.Spider

/**
  * Created by luosl on 2017/11/6.
  */
class Application(val cliProp: CliProp) extends Logging with Runnable{

  private val confLoader:ConfLoader = ConfLoader(cliProp.confPath.getOrElse("spiderConf"))
  private val monitor:HttpSpiderMonitor = new HttpSpiderMonitor(
    cliProp.host.getOrElse("0.0.0.0"),
    cliProp.port.getOrElse(9000)
  )
  private val taskMonitor:TaskMonitor = new TaskMonitor()

  def startService(): Unit ={
    val groupConfigs:Map[Boolean, Array[SpiderConf]] = confLoader.listAllConf().groupBy(_.enabled)
    groupConfigs.getOrElse(false,Array.empty[SpiderConf]).foreach{ conf=>
      logInfo(s"配置文件[path=${conf.path}]为close状态，如果要开启请检查配置[<spider enable='true'>]")
    }
    val enabledSpiders:Array[SpiderConf] = groupConfigs.getOrElse(true,Array.empty[SpiderConf])
    logInfo(s"载配置文件加载完毕!!")
    groupConfigs.getOrElse(true,Array.empty[SpiderConf]).foldLeft(1){ (count,conf)=>
      logInfo(s"正在提交爬虫任务[path=${conf.path}],当前进度:$count / ${enabledSpiders.length}")
      val spider:Spider = SpiderCreater.createSpider(conf)
      taskMonitor.submitSpiderTask(spider,conf)
      monitor.register(spider,conf)
      count + 1
    }
    val thread:Thread = new Thread(this)
    thread.setDaemon(true)
    thread.start()
  }



  override def run(): Unit = {
    while(taskMonitor.runTaskNumber()>0){
      TimeUnit.SECONDS.sleep(2)
    }
    monitor.shutdown()
    taskMonitor.shutdown()
  }
}

object Application {

  def main(args: Array[String]): Unit = {
    val cli:Cli = Cli(args:_*)
    new Application(cli.cliProp()).startService()
  }

}
