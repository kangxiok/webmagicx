package org.luosl.webmagicx.handler

import org.luosl.webmagicx.Utils.Logging
import org.luosl.webmagicx.conf.{Props, SpiderConf}
import us.codecraft.webmagic.handler.Handler

/**
  * Created by luosl on 2017/12/18.
  */
abstract class BaseHandler(sc:SpiderConf, props:Props) extends Handler with Logging{

}
