package org.luosl.webmagicx.listeners
import java.util.concurrent.atomic.AtomicInteger

import play.api.libs.json.{JsObject, Json}
import us.codecraft.webmagic.{Page, ResultItems, Task}

/**
  * Created by luosl on 2017/12/15.
  */
class GeneralProcessorListener(clazz:Class[_]) extends ProcessorListener{

  val name:String = clazz.getSimpleName

  val successCount:AtomicInteger = new AtomicInteger(0)

  val errorCount:AtomicInteger = new AtomicInteger(0)

  val skipCount:AtomicInteger = new AtomicInteger(0)

  override def onSuccess(page: Page): Unit = successCount.getAndIncrement()

  override def onError(page: Page): Unit = errorCount.getAndIncrement()

  override def onSkip(page: Page): Unit = skipCount.getAndIncrement()

  def createJsonMsg(): JsObject ={
    Json.obj(
      "className" -> name,
      "successCount" -> successCount.get(),
      "errorCount" -> errorCount.get(),
      "skipCount" -> skipCount.get()
    )
  }

}
