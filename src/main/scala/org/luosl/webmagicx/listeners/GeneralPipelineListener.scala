package org.luosl.webmagicx.listeners

import java.util.concurrent.atomic.AtomicInteger

import play.api.libs.json.{JsObject, Json}
import us.codecraft.webmagic.{ResultItems, Task}

/**
  * Created by luosl on 2017/12/15.
  */
class GeneralPipelineListener(clazz:Class[_]) extends PipelineListener{

  val name:String = clazz.getSimpleName

  val successCount:AtomicInteger = new AtomicInteger(0)

  val errorCount:AtomicInteger = new AtomicInteger(0)

  val skipCount:AtomicInteger = new AtomicInteger(0)

  override def onSuccess(resultItems: ResultItems, task: Task): Unit = successCount.getAndIncrement()

  override def onError(resultItems: ResultItems, task: Task): Unit = errorCount.getAndIncrement()

  override def onSkip(resultItems: ResultItems, task: Task): Unit = skipCount.getAndIncrement()

  def createJsonMsg(): JsObject ={
    Json.obj(
      "className" -> name,
      "successCount" -> successCount.get(),
      "errorCount" -> errorCount.get(),
      "skipCount" -> skipCount.get()
    )
  }

}
