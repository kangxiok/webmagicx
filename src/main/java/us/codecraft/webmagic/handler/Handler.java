package us.codecraft.webmagic.handler;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.ResultItems;

/**
 * Created by luosl on 2017/12/18.
 */
public interface Handler {
    public void process(Page page, ResultItems items);
}
